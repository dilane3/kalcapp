import React from 'react';
import {View, StyleSheet, Image} from 'react-native';

function Home({ navigation }) {
  return (
    <View style={styles.container}>
      <Image source={require("../images/kalcApp.png")} style={styles.image} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: 300,
    height: 300
  }
});


export default Home;
