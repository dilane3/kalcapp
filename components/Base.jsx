import React from 'react';
import {View, Text, TextInput, TouchableOpacity, StyleSheet, StatusBar} from 'react-native';
import Home from './Home';

class Base extends React.Component {
  constructor() {
    super();

    this.state = {
      expression: "",
      response: "",
      home: true
    }

    this.element = null;
    this.pile = null;
  }

  componentDidMount() {
    let time = setTimeout(() => {
      this.setState({ home: false });
    }, 2000);
  }

  updateExpression = (caractere) => {
    let expression = this.state.expression;

    if (caractere !== "") {
      if (expression.length === 0 && Number(caractere)) {
        expression += caractere;
      } else if (expression.length !== 0) {
        expression += caractere;
      }
    } else {
      expression = expression.substr(0, expression.length-1);

      if (expression === '') {
        this.setState({ response: '' });
      }
    }

    this.setState({ expression });
    console.log(caractere);

    let time = setTimeout(() => {
      if (Number(expression[expression.length-1]) || Number(expression[expression.length-1]) === 0) {
        expression = expression.replace("×", "*");
        expression = expression.replace("÷", "/");

        console.log(expression);

        this.setState({ response: new String(eval(expression)) });
      }
    }, 0);
  }

  displayButton = (value, id) => {
    return (
      <TouchableOpacity key={id} style={styles.btnNumber} onPress={() => this.updateExpression(value)}>
        <Text style={{fontSize: 40, color: "white"}}>{value}</Text>
      </TouchableOpacity>
    )
  }

  evaluate = (expression, operator) => {
    let op = expression.indexOf(operator);
    let nb1 = "";
    let nb2 = "";

    while (op !== -1) {
      let i, j;
      for (i = op+1; i <= expression.length-1; i++) {
        nb1 += expression[i];

        if (expression[i+1] === "+" || expression[i+1] === "-" || expression[i+1] === "×" || expression[i+1] === "÷") {
          i += 1;
          break;
        }
      }

      for (j = op-1; j >= 0; j--) {
        nb2 += expression[j];

        if (expression[j-1] === "+" || expression[j-1] === "-" || expression[j-1] === "×" || expression[j-1] === "÷") {
          j -= 1;
          break;
        }
      }

      if (operator === "×") {
        expression = expression.slice(0, j+1) + (Number(nb1.split('').reverse().join('')) * Number(nb2.split('').reverse().join(''))) + expression.slice(i, expression.length);
      } else if (operator === "÷") {
        expression = expression.slice(0, j+1) + (Math.floor((Number(nb2.split('').reverse().join('')) / Number(nb1.split('').reverse().join('')))) * 100) + expression.slice(i, expression.length);
      } else if (operator === "-") {
        expression = expression.slice(0, j+1) + (Number(nb2.split('').reverse().join('')) - Number(nb1.split('').reverse().join(''))) + expression.slice(i, expression.length);
      } else {
        expression = expression.slice(0, j+1) + (Number(nb2.split('').reverse().join('')) + Number(nb1.split('').reverse().join(''))) + expression.slice(i, expression.length);
      }

      console.log(nb1+ " - "+ nb2);
      console.log(expression);
      nb2 = '';
      nb1 = '';

      op = expression.indexOf(operator);
    }

    return expression;
  }

  calculate = (expression) => {
    let expression1 = this.evaluate(expression, "×");
    expression1 = this.evaluate(expression1, "÷");

    return eval(expression1);
  }

  back = () => {
    let expression = this.state.expression;

    expression = expression.substr(0, expression.length-1);
    this.setState({ expression });

    this.updateExpression("");
  }

  giveAnswer = () => {
    let response = this.state.response;

    this.setState({ response: '', expression: response });
  }

  render() {
    return (
      <>
        {
          this.state.home
            ? <Home />
            : (
               <View style={styles.container}>
                <StatusBar
                  backgroundColor="blue"
                />
                <View style={styles.header}>
                  <Text style={[styles.headerIcon, {fontWeight: '700', fontSize: 25}]}>KalcApp</Text>
                </View>
                <View style={styles.mainSection}>
                  <View style={styles.screen}>
                    <View style={{ height: 60, paddingHorizontal: 20, flexDirection: "row", alignItems: 'center' }}>
                      <Text style={{fontSize: 30, marginLeft: "auto"}}>{ this.state.expression }</Text>
                    </View>

                    <Text style={{ marginTop: "auto", marginBottom: 10, marginLeft: "auto", marginRight: 20, fontSize: 25, color: "#6a6a6a" }}>{this.state.response}</Text>
                  </View>

                  <View style={styles.keyboard}>
                    <View style={styles.keyboardNumber}>

                      <View style={styles.btnRow}>
                        {
                          [7, 8, 9].map((number, id) => {
                            return this.displayButton(number, id);
                          })
                        }
                      </View>
                      <View style={styles.btnRow}>
                        {
                          [4, 5, 6].map((number, id) => {
                            return this.displayButton(number, id);
                          })
                        }
                      </View>
                      <View style={styles.btnRow}>
                        {
                          [1, 2, 3].map((number, id) => {
                            return this.displayButton(number, id);
                          })
                        }
                      </View>
                      <View style={styles.btnRow}>
                        {
                          ['.', 0].map((number, id) => {
                            return this.displayButton(number, id);
                          })
                        }
                        <TouchableOpacity style={styles.btnNumber} onPress={() => this.giveAnswer()}>
                          <Text style={{fontSize: 40, color: "white"}}>=</Text>
                        </TouchableOpacity>
                      </View>

                    </View>
                    <View style={styles.keyboardOperator}>

                      <TouchableOpacity style={styles.btnNumber} onPress={() => {this.updateExpression('')}}>
                        <Text style={{fontSize: 40, color: "white"}}>←</Text>
                      </TouchableOpacity>

                      {
                        ['÷', '×', '+', '-'].map((number, id) => {
                          return this.displayButton(number, id);
                        })
                      }

                    </View>
                    <View style={styles.keyboardOption}></View>
                  </View>
                </View>
              </View>
            )
        }
      </>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: "column",
    alignItems: 'flex-start',
    justifyContent: 'center'
  },
  header: {
    height: 60,
    width: "100%",
    backgroundColor: "#3b4eff",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 20
  },
  headerIcon: {
    fontSize: 20,
    color: "white"
  },
  mainSection: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "white",
    width: "100%"
  },
  keyboard: {
    flex: 3,
    width: "100%",
    backgroundColor: "blue",
    flexDirection: "row",
    justifyContent: "flex-start"
  },
  screen: {
    flex: 1,
    width: "100%"
  },
  keyboardNumber: {
    flex: 6,
    backgroundColor: "#6a6a6a",
    flexDirection: "column",
  },
  keyboardOperator: {
    flex: 2,
    backgroundColor: "#969696"
  },
  keyboardOption: {
    width: 25,
    backgroundColor: "#2eff66"
  },
  btnRow: {
    flex: 1,
    flexDirection: "row"
  },
  btnNumber: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default Base;
